# Laravel Builder Image

[![](https://images.microbadger.com/badges/image/liquidfish/laravel-builder.svg)](https://microbadger.com/images/liquidfish/laravel-builder "Get your own image badge on microbadger.com")

These are a set of images with the needed dependencies installed without having to rebuild them every CI build.

## Arguments

### Install Flags

| Build Arg         | Default |
|-------------------|---------|
| INSTALL_BCMATH    | true    |
| INSTALL_EXIF      | false   |
| INSTALL_GD        | false   |
| INSTALL_ICONV     | false   |
| INSTALL_IMAGICK   | false   |
| INSTALL_INTL      | false   |
| INSTALL_MCRYPT    | false   |
| INSTALL_MYSQL     | false   |
| INSTALL_OPCACHE   | true    |
| INSTALL_PCNTL     | false   |
| INSTALL_PGSQL     | true    |
| INSTALL_REDIS     | true    |
| INSTALL_V8        | false   |
| INSTALL_XDEBUG    | false   |
| XDEBUG_VERSION    | false   |
| INSTALL_ZIP       | false   |

#### Notes

##### `INSTALL_V8`

For `INSTALL_V8` to work, you need to use the `-v8` tags of this image. The build process for it is complex and can't be done in the context of `ONBUILD` while still maintaining Alpine-based images.

##### `XDEBUG_VERSION`

`XDEBUG_VERSION` depends on `INSTALL_XDEBUG`. It's mainly for PHP 5.6 apps, where the latest xdebug refuses to install. The version must replace `.` with `_`. So for version 2.5.5, it should be set to `2_5_5`.

### Dependencies

| Build Arg | Description                              |
|-----------|------------------------------------------|
| DEPS      | Other dependencies to install from `apk` |
